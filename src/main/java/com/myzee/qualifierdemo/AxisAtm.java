package com.myzee.qualifierdemo;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("axis")
public class AxisAtm implements ATM {

	@Override
	public String atmBranch() {
		return "axis atm";
	}

}
