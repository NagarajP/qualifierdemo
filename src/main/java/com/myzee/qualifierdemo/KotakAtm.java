package com.myzee.qualifierdemo;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("kotak")
public class KotakAtm implements ATM {

	@Override
	public String atmBranch() {
		return "Kotak atm";
	}

}
