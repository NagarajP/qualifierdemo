package com.myzee.qualifierdemo;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AtmConfig {

/*
 * either enable/uncomment below two beans or use @component and @qualifier at 
 * AxisAtm.java and KotakAtm.java classes
 */

//	@Bean
//	@Qualifier("axis")
//	public ATM getAxisAtm() {
//		return new AxisAtm();
//	}
//	
//	@Bean
//	@Qualifier("kotak")
//	public ATM getKotakAtm() {
//		return new KotakAtm();
//	}
}
