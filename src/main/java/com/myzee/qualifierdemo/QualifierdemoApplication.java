package com.myzee.qualifierdemo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
@RequestMapping(value = "/myzee")
public class QualifierdemoApplication {

	private final static Logger logger = LogManager.getLogger(QualifierdemoApplication.class);
	
	@Autowired
	@Qualifier("axis")
	ATM atm;
	public static void main(String[] args) {
		SpringApplication.run(QualifierdemoApplication.class, args);
	}

	@GetMapping(value = "/call", produces = "application/xml")
	public String call() {
		System.out.println("calling getmapping");
		logger.info("calling getmapping");
		String name = atm.atmBranch();
//		return "<html><body>" + name + "</body></html>";
		return "<name>" + name + "</name>";
		
	}
}
